package main

import (
	"flag"
	"log"
	"os"
	"path/filepath"

	"strings"

	"sync"

	"time"

	"github.com/casept/id3-go"
)

var (
	wg sync.WaitGroup

	dir   = flag.String("dir", "", "-dir /path/to/dir")
	clear = flag.Bool("clear", false, "-clear true|false")

	mCounter int
)

func main() {
	flag.Parse()
	if *dir == "" {
		log.Fatal("Wrong args. Type 'grappa -h' for help")
	}

	timeStart := time.Now()
	filepath.Walk(*dir,
		func(path string, info os.FileInfo, err error) error {
			if !isMp3(info.Name()) || info.IsDir() {
				return nil
			}

			mCounter++
			wg.Add(1)
			go makeTag(path, info.Name())

			return nil
		})

	wg.Wait()
	timeDelta := time.Now().Sub(timeStart)
	log.Printf("Processed %v files in %s ", mCounter, timeDelta)
}

func isMp3(name string) bool {
	return "mp3" == name[len(name)-3:]
}

func parseName(name string) (string, string) {
	var artist, title string
	tmp1 := strings.Replace(name, ".mp3", "", 1)
	tmp2 := strings.Split(tmp1, " - ")
	if len(tmp2) == 2 {
		artist = tmp2[0]
		title = tmp2[1]
	} else {
		artist = strings.Join(tmp2[:len(tmp2)-1], " - ")
		title = tmp2[len(tmp2)-1]
	}
	return artist, title
}

func makeTag(path string, name string) {
	var artist, title string
	if !*clear {
		artist, title = parseName(name)
	}
	file, err := id3.Open(path)
	if err != nil {
		log.Printf("Could not open: %v", path)
		wg.Done()
		return
	}
	file.SetArtist(artist)
	file.SetTitle(title)
	file.Close()
	wg.Done()
}
